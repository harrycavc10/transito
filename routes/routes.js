var express = require('express');
var router = express.Router();
var controllers = require('.././controllers');

/* GET home page. */
router.get('/', controllers.homecontroller.index);

//rutas para propietario
router.get('/propietario',controllers.propietariocontroller.getPropietarios);
router.get('/propietario/nuevo',controllers.propietariocontroller.getNuevoPropietario);
router.post('/propietario/crearpropietario',controllers.propietariocontroller.postNuevoPropietario);
router.post('/propietario/eliminarpropietario',controllers.propietariocontroller.eliminarPropietario);
router.get('/propietario/modificar/:cedula',controllers.propietariocontroller.getModificarPropietario);
router.post('/propietario/editarpropietario',controllers.propietariocontroller.postModificarPropietario);


// rutas para vehiculo

router.get('/vehiculo',controllers.vehiculocontroller.getvehiculos);
router.get('/vehiculo/nuevo',controllers.vehiculocontroller.getNuevovehiculo);
router.post('/vehiculo/crearvehiculo',controllers.vehiculocontroller.postNuevovehiculo);
router.post('/vehiculo/eliminarvehiculo',controllers.vehiculocontroller.eliminarvehiculo);
router.get('/vehiculo/modificar/:id_placa',controllers.vehiculocontroller.getModificarvehiculo);
router.post('/vehiculo/editarvehiculo',controllers.vehiculocontroller.postModificarvehiculo);


//rutas para multa

router.get('/multa',controllers.multacontroller.getmultas);
router.get('/multa/nuevo',controllers.multacontroller.getNuevomulta);
router.post('/multa/crearmulta',controllers.multacontroller.postNuevomulta);
router.post('/multa/eliminarmulta',controllers.multacontroller.eliminarmulta);
router.get('/multa/modificar/:id_multa',controllers.multacontroller.getModificarmulta);
router.post('/multa/editarmulta',controllers.multacontroller.postModificarmulta);

router.get('/multa/multaXpropietario',controllers.multacontroller.getmultasxpropietario);
router.post('/multa/consulta',controllers.multacontroller.postmultasxpropietario);



module.exports = router;
