var mysql = require('mysql');
//propietario controller

module.exports = {

	//funciones del controlador 

	getPropietarios : function(req, res, next){
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var propietarios = null;
		db.query('select * from propietario',function(err,rows,fields){
			if(err) throw err;
			propietarios = rows;
			db.end();


			res.render('propietario/propietario', {propietarios:propietarios});

		});
	},
	getNuevoPropietario : function(req,res,next){
		res.render('propietario/nuevo');
	},
	postNuevoPropietario : function(req,res,next){
		var propietario = {
			cedula : req.body.cedula,
			nombre : req.body.nombre,
			apellido : req.body.apellido
		}
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		db.query('insert into propietario set ?',propietario,function(err,rows,fields){
			if(err) throw err;
			propietarios = rows;
			db.end();

			});
		res.render('propietario/nuevo',{info: 'El propietario due creado correctamente'});


	},
	eliminarPropietario : function(req, res, next){
		var cedula = req.body.cedula;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var respuesta = {res: false};
		db.query('delete from propietario where cedula = ?',cedula,function(err,rows,fields){
			if(err) throw err;
			
			db.end();
			respuesta.res=true;
			res.json(respuesta);

		});
	},
	getModificarPropietario : function(req, res, next){
		var cedula = req.params.cedula;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var propietario = null;
		db.query('select * from propietario where cedula = ?',cedula,function(err,rows,fields){
			if(err) throw err;
			propietario = rows;
			db.end();
			res.render('propietario/modificar',{propietario:propietario});

});
	},

	postModificarPropietario : function(req, res, next){

		var propietario = {
			nombre : req.body.nombre,
			apellido : req.body.apellido
		};

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('UPDATE propietario SET ? WHERE ?', [propietario, {cedula : req.body.cedula}], function(err, rows, fields){
			if(err) throw err;
			db.end();
		});

		res.redirect('/propietario');

	}


}