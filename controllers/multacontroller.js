var mysql = require('mysql');
var dateFormat = require('dateformat');
//multa controller

module.exports = {

	//funciones del controlador 

	getmultas : function(req, res, next){
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var multas = null;
		db.query('select * from multa',function(err,rows,fields){
			if(err) throw err;
			multas = rows;
			db.end();


			res.render('multa/multa', {multas:multas});

		});
	},
	getNuevomulta : function(req,res,next){
		res.render('multa/nuevo');
	},
	postNuevomulta : function(req,res,next){
		var Cedula=null;
		var placa =req.body.id_placa;

		
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		db.query('select cedula from vehiculo where id_placa = ?',placa,function(err,rows,fields){
			if(err) throw err;
			Cedula = rows;

var ced=Cedula[0].cedula;
		var fechaactual = new Date();
		var fecha = dateFormat(fechaactual, 'yyyy-mm-dd');

		var multa = {
			fecha_multa : fecha,
			id_placa : req.body.id_placa,
			descripcion_multa: req.body.descripcion_multa,
			estado_multa : 0,
			valor_multa : req.body.valor_multa,
			cedula : ced
		}

console.log(multa);


		db.query('insert into multa set ?',multa,function(err,rows,fields){
			if(err) throw err;
			multas = rows;
			db.end();

			});



			});

		
		res.render('multa/nuevo',{info: 'El registro de la multa fue creado correctamente'});


	},
	eliminarmulta : function(req, res, next){
		var id_multa = req.body.id_multa;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var respuesta = {res: false};
		db.query('delete from multa where id_multa = ?',id_multa,function(err,rows,fields){
			if(err) throw err;
			
			db.end();
			respuesta.res=true;
			res.json(respuesta);

		});
	},
	getModificarmulta : function(req, res, next){
		var id_multa = req.params.id_multa;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var multa = null;
		db.query('select * from multa where id_multa = ?',id_multa,function(err,rows,fields){
			if(err) throw err;
			multa = rows;
			db.end();
			res.render('multa/modificar',{multa:multa});

});
	},

	postModificarmulta : function(req, res, next){

		var Cedula=null;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		db.query('select cedula from vehiculo where id_placa = ?',req.body.id_placa,function(err,rows,fields){
			if(err) throw err;
			Cedula = rows;
		var multa = {
			id_placa : req.body.id_placa,
			descripcion_multa: req.body.descripcion_multa,
			estado_multa : req.body.estado_multa,
			valor_multa : req.body.valor_multa,
			cedula : Cedula[0].cedula
		};


		db.query('UPDATE multa SET ? WHERE ?', [multa, {id_multa : req.body.id_multa}], function(err, rows, fields){
			if(err) throw err;
			db.end();
		});
		});

		res.redirect('/multa');

	},
	getmultasxpropietario : function(req, res, next){
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var multas = null;
		db.query('select * from multa',function(err,rows,fields){
			if(err) throw err;
			multas = rows;
			db.end();


			res.render('multa/multaXpropietario', {multas:multas});

		});
	},
	postmultasxpropietario : function(req, res, next){
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var multas = null;
		console.log(req.body.cedula);
		db.query('select * from multa where cedula = ?',req.body.cedula,function(err,rows,fields){
			if(err) throw err;
			multas = rows;
			db.end();


			res.render('multa/consulta', {multas:multas});

		});
	}


}