var mysql = require('mysql');
//vehiculo controller

module.exports = {

	//funciones del controlador 

	getvehiculos : function(req, res, next){
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var vehiculos = null;
		db.query('select * from vehiculo',function(err,rows,fields){
			if(err) throw err;
			vehiculos = rows;
			db.end();


			res.render('vehiculo/vehiculo', {vehiculos:vehiculos});

		});
	},
	getNuevovehiculo : function(req,res,next){
		res.render('vehiculo/nuevo');
	},
	postNuevovehiculo : function(req,res,next){
		var vehiculo = {
			id_placa : req.body.id_placa,
			modelo : req.body.modelo,
			cedula : req.body.cedula,
			año : req.body.año
		}
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		db.query('insert into vehiculo set ?',vehiculo,function(err,rows,fields){
			if(err) throw err;
			vehiculos = rows;
			db.end();

			});
		res.render('vehiculo/nuevo',{info: 'El vehiculo fue creado correctamente'});


	},
	eliminarvehiculo : function(req, res, next){
		var id_placa = req.body.id_placa;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var respuesta = {res: false};
		db.query('delete from vehiculo where id_placa = ?',id_placa,function(err,rows,fields){
			if(err) throw err;
			
			db.end();
			respuesta.res=true;
			res.json(respuesta);

		});
	},
	getModificarvehiculo : function(req, res, next){
		var id_placa = req.params.id_placa;
		var config = require('.././database/config');
		var db = mysql.createConnection(config);
		db.connect();
		var vehiculo = null;
		db.query('select * from vehiculo where id_placa = ?',id_placa,function(err,rows,fields){
			if(err) throw err;
			vehiculo = rows;
			db.end();
			res.render('vehiculo/modificar',{vehiculo:vehiculo});

});
	},

	postModificarvehiculo : function(req, res, next){

		var vehiculo = {
			modelo : req.body.modelo,
			cedula : req.body.cedula,
			año : req.body.año
		};

		var config = require('.././database/config');

		var db = mysql.createConnection(config);
		db.connect();

		db.query('UPDATE vehiculo SET ? WHERE ?', [vehiculo, {id_placa : req.body.id_placa}], function(err, rows, fields){
			if(err) throw err;
			db.end();
		});

		res.redirect('/vehiculo');

	}


}