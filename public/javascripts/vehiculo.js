$(function(){


	//funcion ajax eliminar producto
	$('#tbl-vehiculo #btn-eliminar').click(function(){				

		var elemento = $(this);	
		var id_placa = elemento.parent().parent().find('#id_placa').text();

		var confirmar = confirm('Desea eliminar el vehiculo');

		if(confirmar){
			$.ajax({
				url : 'http://localhost:8888/vehiculo/eliminarvehiculo',
				method : 'post',
				data : {id_placa : id_placa},
				success : function(res){
					if(res.res){
						elemento.parent().parent().remove();
					}
				}
			});
		}		
		
	});


});