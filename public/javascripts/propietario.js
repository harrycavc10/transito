$(function(){


	//funcion ajax eliminar producto
	$('#tbl-propietario #btn-eliminar').click(function(){				

		var elemento = $(this);	
		var cedula = elemento.parent().parent().find('#cedula').text();

		var confirmar = confirm('Desea eliminar el propietario');

		if(confirmar){
			$.ajax({
				url : 'http://localhost:8888/propietario/eliminarpropietario',
				method : 'post',
				data : {cedula : cedula},
				success : function(res){
					if(res.res){
						elemento.parent().parent().remove();
					}
				}
			});
		}		
		
	});


});