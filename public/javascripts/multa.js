$(function(){


	//funcion ajax eliminar producto
	$('#tbl-multa #btn-eliminar').click(function(){				

		var elemento = $(this);	
		var id_multa = elemento.parent().parent().find('#id_multa').text();

		var confirmar = confirm('Desea eliminar el multa');

		if(confirmar){
			$.ajax({
				url : 'http://localhost:8888/multa/eliminarmulta',
				method : 'post',
				data : {id_multa : id_multa},
				success : function(res){
					if(res.res){
						elemento.parent().parent().remove();
					}
				}
			});
		}		
		
	});


});